from python_graphql_client import GraphqlClient

class GraphQLTestClient:
    def __init__(self, end_point, headers=None):
        self.end_point = end_point
        if headers:
            self.client = GraphqlClient(endpoint=end_point, headers=headers)
        else:
            self.client = GraphqlClient(endpoint=end_point)

    def query(self, query, variables=None):
        if variables:
            return self.client.execute(query=query, variables=variables)
        else:
            return self.client.execute(query=query)
