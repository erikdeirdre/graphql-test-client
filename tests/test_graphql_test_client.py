from os import environ
import unittest
from graphqltestclient.graphqltestclient import GraphQLTestClient

class TestTestClass(unittest.TestCase):
    def setUp(self):
        self.url = "https://gitlab.com/api/graphql"
        auth_token = environ.get("AUTH_TOKEN")
        self.header = {"Authorization" : f"Bearer {auth_token}"}

    def test_graphql_not_authenticated(self):
        query = """{
  user(username:"erikdeirdre") {
    id
    name
    username
  }
}
"""
        expected_response = "The resource that you are attempting to access does not exist or you don't have permission to perform this action"
        client = GraphQLTestClient(self.url)
        result = client.query(query)
        self.assertEqual(expected_response, result['errors'][0]['message'])

    def test_graphql_no_variables(self):
        query = """{
  user(username:"erikdeirdre") {
    id
    name
    username
  }
}
"""
        expected_response = {
          "data": {
            "user": {
              "id": "gid://gitlab/User/2408352",
              "name": "erikdeirdre",
              "username": "erikdeirdre"
            }
          }
        }

        client = GraphQLTestClient(self.url, headers=self.header)
        result = client.query(query)
        self.assertEqual(expected_response, result)

    def test_graphql_variables(self):
        query = """query getUser($user: String){
  user(username: $user) {
    id
    name
    username
  }
}
"""
        expected_response = {
          "data": {
            "user": {
              "id": "gid://gitlab/User/2408352",
              "name": "erikdeirdre",
              "username": "erikdeirdre"
            }
          }
        }
        variables = {
          "user": "erikdeirdre"
        }
        client = GraphQLTestClient(self.url, headers=self.header)
        result = client.query(query, variables=variables)
        self.assertEqual(expected_response, result)

if __name__ == '__main__':
    unittest.main()
